import 'package:flutter/material.dart';
import 'pet_card.dart';
import 'pet_model.dart';

class PetList extends StatelessWidget {
  final List<Pet> petList;
  PetList(this.petList);

  @override
  Widget build(BuildContext context) {
    return _buildList(context);
  }

  ListView _buildList(context) {
    return ListView.builder(
      itemCount: petList.length,

      // A callback that will return a widget.
      itemBuilder: (context, i) {
        return PetCard(petList[i]);
      },
    );
  }
}
