import 'package:flutter/material.dart';
import 'pet_model.dart';
import 'pet_list.dart';
import 'pet_theme.dart';
import 'add_pet_form.dart';
import 'package:pet_app/pet_shop.dart';
import 'package:pet_app/pet_camera.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'We Rate Pets!',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        brightness: Brightness.light,
      ),
      home: MyHomePage(title: 'We Rate Pets!'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Pet> petList = []
    ..add(Pet('Sasha', 'Penang',
        'Cool and irritated pug.In Flutter, widgets are rendered by their underlying RenderBox objects. Render boxes are given constraints by their parent, and size themselves within those constraints. Constraints consist of minimum and maximum widths and heights; sizes consist of a specific width and height.'))
    ..add(Pet('Fluffy', 'Penang', 'Baby golden retriever.'))
    ..add(Pet('Ruby', 'Portland, OR, USA', 'Ruby is a very good girl.'))
    ..add(Pet('Rex', 'Seattle, WA, USA', 'Best in 1999'))
    ..add(Pet('Rod Stewart', 'Prague, CZ', 'Star good boy'))
    ..add(Pet('Herbert', 'Dallas, TX, USA', 'A Very Good Boy'))
    ..add(Pet('Buddy', 'North Pole, Earth', 'Self proclaimed human lover.'));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        backgroundColor: Colors.pink,
        actions: [
          IconButton(
            icon: Icon(Icons.add),
            onPressed: _showNewPetForm,
          ),
          IconButton(
            icon: Icon(Icons.shopping_cart),
            onPressed: () => _showPetShopPage(),
          ),
          IconButton(
            icon: Icon(Icons.camera_enhance),
            onPressed: () => _showPetCameraPage(),
          )
        ],
      ),
      body: Container(
        decoration: myBoxDecoration(),
        child: PetList(petList),
      ),
    );
  }

  void _showPetShopPage() {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (BuildContext context) {
      return PetShopPage();
    }));
  }

  void _showPetCameraPage() {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (BuildContext context) {
      return PetCameraPage();
    }));
  }

  // Any time you're pushing a new route and expect that route
  // to return something back to you,
  // you need to use an async function.
  // In this case, the function will create a form page
  // which the user can fill out and submit.
  // On submission, the information in that form page
  // will be passed back to this function.
  Future _showNewPetForm() async {
    Pet newPet = await Navigator.of(context)
        .push(MaterialPageRoute(builder: (BuildContext context) {
      return AddPetFormPage();
    }));

    if (newPet != null) {
      petList.add(newPet);
    }
  }
}
