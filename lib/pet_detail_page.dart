import 'package:flutter/material.dart';
import 'pet_model.dart';
import 'pet_theme.dart';

class PetDetailPage extends StatefulWidget {
  final Pet pet;

  PetDetailPage(this.pet);

  @override
  _PetDetailPageState createState() => _PetDetailPageState();
}

class _PetDetailPageState extends State<PetDetailPage> {
  final double petAvatarSize = 150.0;
  double _sliderValue = 10.0;

  Widget get addYourRating {
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 30.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              // it will expand as much as it can until
              // the all the space is taken up.
              Flexible(
                flex: 1,
                child: Slider(
                  activeColor: Colors.pink,
                  min: 0.0,
                  max: 10.0,
                  onChanged: (newRating) {
                    setState(() => _sliderValue = newRating);
                  },
                  value: _sliderValue,
                ),
              ),
              // This is the part that displays the value of the slider.
              Container(
                alignment: Alignment.center,
                child: Text('${_sliderValue.toInt()}',
                    style: Theme.of(context).textTheme.display1),
              ),
            ],
          ),
        ),
        submitRatingButton,
      ],
    );
  }

  // Just like a route, this needs to be async, because it can return
  // information when the user interacts.
  Future<Null> _ratingErrorDialog() async {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Error!'),
            content: Text("They're good pet!"),
            actions: <Widget>[
              FlatButton(
                child: Text('Try again'),
                onPressed: () => Navigator.of(context).pop(),
              )
            ],
          );
        });
  }

  Widget get submitRatingButton {
    return RaisedButton(
      onPressed: () {
        if (_sliderValue < 5) {
          _ratingErrorDialog();
        } else {
          setState(() => widget.pet.rating = _sliderValue.toInt());
        }
      },
      child: Text('Submit', style: TextStyle(color: Colors.white)),
      color: Colors.pink,
    );
  }

  Widget get petImage {
    return Hero(
        tag: widget.pet,
        child: Container(
          height: petAvatarSize,
          width: petAvatarSize,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            boxShadow: [
              const BoxShadow(
                  offset: const Offset(1.0, 2.0),
                  blurRadius: 2.0,
                  spreadRadius: -1.0,
                  color: const Color(0x33000000)),
              const BoxShadow(
                  offset: const Offset(2.0, 1.0),
                  blurRadius: 3.0,
                  spreadRadius: 0.0,
                  color: const Color(0x24000000)),
              const BoxShadow(
                  offset: const Offset(3.0, 1.0),
                  blurRadius: 4.0,
                  spreadRadius: 2.0,
                  color: const Color(0x1F000000)),
            ],
            image: DecorationImage(
              fit: BoxFit.cover,
              image: NetworkImage(widget.pet.imageUrl),
            ),
          ),
        ));
  }

  Widget get rating {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Icon(
          Icons.star,
          size: 40.0,
          color: Colors.pink,
          
        ),
        Text(' ${widget.pet.rating} / 10',
            style: Theme.of(context).textTheme.display2),
      ],
    );
  }

  Widget get petProfile {
    return Container(
      decoration: myBoxDecoration(),
      padding: EdgeInsets.symmetric(vertical: 32.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: getPadding(0.0, 15.0),
            child: petImage,
          ),
          Padding(
            padding: getPadding(0.0, 5.0),
            child: Text(
              '${widget.pet.name}',
              style: TextStyle(fontSize: 32.0),
            ),
          ),
          Padding(
              padding: getPadding(0.0, 5.0),
              child: Text(
                widget.pet.location,
                style: TextStyle(fontSize: 20.0),
              )),
          Container(
            padding: getPadding(20.0, 5.0),
            child: Text(widget.pet.description, textAlign: TextAlign.center),
          ),
          rating,
          addYourRating
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text('Meet ${widget.pet.name}'),
          backgroundColor: Colors.pink,
        ),
        body: ListView(
          children: <Widget>[petProfile],
        ));
  }
}
