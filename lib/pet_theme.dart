import 'package:flutter/material.dart';

BoxDecoration myBoxDecoration() {
  return BoxDecoration(
      gradient: LinearGradient(
    begin: Alignment.topRight,
    end: Alignment.bottomLeft,
    stops: [0.1, 0.9],
    colors: [
      Colors.white,
      Colors.white70,
    ],
  ));
}

getPadding(double n, double m) {
  return EdgeInsets.symmetric(horizontal: n, vertical: m);
}
