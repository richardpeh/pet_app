import 'package:flutter/material.dart';

class PetShopPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _PetShopPageState();
  }
}

class _item {
  String title;
  String imgPath;
  bool fav;

  _item(this.title, this.imgPath, this.fav);
}

class _PetShopPageState extends State<PetShopPage>
    with SingleTickerProviderStateMixin {
  TabController _controller;

  List<_item> itemList = []
    ..add(_item("FinnNavian", 'assets/ottoman.jpg', true))
    ..add(_item("FinnNavian", 'assets/anotherchair.jpg', false))
    ..add(_item("FinnNavian", 'assets/chair.jpg', false));

  @override
  void initState() {
    super.initState();
    _controller = new TabController(length: 4, vsync: this);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pet shop!'),
        backgroundColor: Colors.pink,
      ),
      body: _myListView,
      // floatingActionButton: FloatingActionButton(
      //     tooltip: 'Navigate back',
      //     child: Icon(Icons.arrow_back),
      //     onPressed: () => Navigator.of(context).pop(),
      //     backgroundColor: Colors.pink),
      bottomNavigationBar: _myBottomNavigationBar,
    );
  }

  ListView get _myListView {
    return ListView(
      children: <Widget>[
        Column(children: <Widget>[
          _myStack,
          _myMidBar,
          itemCard(itemList[0]),
          itemCard(itemList[1]),
          itemCard(itemList[2])
        ]),
      ],
    );
  }

  Container get _myMidBar {
    return Container(
      height: 75.0,
      child: Material(
        elevation: 5.0,
        color: Colors.grey[200],
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                FlutterLogo(size: 50, colors: Colors.pink),
                Text(
                  'Food',
                  style: TextStyle(fontSize: 12.0),
                )
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                FlutterLogo(size: 50, colors: Colors.pink),
                Text(
                  'Toy',
                  style: TextStyle(fontSize: 12.0),
                )
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                FlutterLogo(size: 50, colors: Colors.pink),
                Text(
                  'Dress',
                  style: TextStyle(fontSize: 12.0),
                )
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                FlutterLogo(size: 50, colors: Colors.pink),
                Text(
                  'Hygiene',
                  style: TextStyle(fontSize: 12.0),
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  Stack get _myStack {
    return Stack(children: <Widget>[
      Container(
        height: 250.0,
        width: double.infinity,
        color: Colors.grey.withOpacity(0.3),
      ),
      _bubble(50.0, 100.0, 400.0),
      _stackOtherStuff,
    ]);
  }

  Positioned _bubble(double x, double y, double diameter) {
    return Positioned(
      bottom: x,
      right: y,
      child: Container(
        height: diameter,
        width: diameter,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(diameter / 2),
            color: Colors.grey.withOpacity(0.1)),
      ),
    );
  }

  Widget get _myBottomNavigationBar {
    return Material(
      color: Colors.white,
      child: TabBar(
        controller: _controller,
        indicatorColor: Colors.pink,
        tabs: <Widget>[
          Tab(
            icon: Icon(Icons.event, color: Colors.grey),
          ),
          Tab(
            icon: Icon(Icons.timelapse, color: Colors.grey),
          ),
          Tab(
            icon: Icon(Icons.shopping_basket, color: Colors.grey),
          ),
          Tab(
            icon: Icon(Icons.person_outline, color: Colors.grey),
          )
        ],
      ),
    );
  }

  Column get _stackOtherStuff {
    return Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly, // Align left
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          // Vertical Space
          SizedBox(
            height: 15.0,
          ),
          _myTopRow,
          // Vertical Space
          SizedBox(
            height: 50.0,
          ),
          Padding(
            padding: EdgeInsets.only(left: 15.0),
            child: Text(
              'Hello, Richard!',
              textAlign: TextAlign.left,
              style: TextStyle(fontSize: 30.0, fontWeight: FontWeight.bold),
            ),
          ),
          // Vertical Space
          SizedBox(
            height: 15.0,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15.0),
            child: Text(
              'What do you want to buy?',
              style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(
            height: 25.0,
          ),
          _mySearchBox,
          SizedBox(
            height: 15.0,
          ),
        ]);
  }

  Widget get _myTopRow {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        // Horizontal Space
        // SizedBox(width: 15.0,),
        Container(
            height: 50.0,
            width: 50.0,
            margin: EdgeInsets.only(left: 15.0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(25.0),
              border: Border.all(
                  color: Colors.pink, style: BorderStyle.solid, width: 2.0),
            )),
        // Horizontal Space
        // SizedBox(
        //   // Btw, MediaQuery is good stuff!
        //   width: MediaQuery.of(context).size.width - 150.0,
        // ),
        Container(
          margin: EdgeInsets.only(right: 15.0),
          child: IconButton(
              icon: Icon(Icons.menu),
              onPressed: () {},
              color: Colors.pink,
              iconSize: 30.0),
        ),
        // SizedBox(width: 15.0,),
      ],
    );
  }

  Widget get _mySearchBox {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15.0),
      child: Material(
        elevation: 5.0,
        borderRadius: BorderRadius.circular(5.0),
        child: TextFormField(
          decoration: InputDecoration(
              border: InputBorder.none,
              prefixIcon: Icon(
                Icons.search,
                color: Colors.pink,
                size: 30.0,
              ),
              contentPadding: EdgeInsets.all(15.0),
              hintText: 'Search',
              hintStyle:
                  TextStyle(color: Colors.pink.withOpacity(0.3), fontSize: 20)),
        ),
      ),
    );
  }

  Widget itemCard(_item item) {
    String title = item.title;
    String imgPath = item.imgPath;
    bool isFavorite = item.fav;
    double column_empty = 20.0;
    double total_horizontal_margin = 20.0;
    double column_1 = (MediaQuery.of(context).size.width -
            column_empty -
            total_horizontal_margin) *
        2 /
        5;
    double column_2 = column_1 / 2 * 3;

    return Padding(
      padding: EdgeInsets.all(10.0),
      child: Container(
        height: 150.0,
        width: double.infinity,
        color: Colors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Container(
              width: column_1,
              height: 150.0,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage(imgPath), fit: BoxFit.cover)),
            ),
            SizedBox(
              width: column_empty,
            ),
            Container(
              width: column_2,
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(left: 0),
                        child: Text(
                          title,
                          style: TextStyle(
                              fontSize: 17.0, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 10.0),
                  Container(
                    width: column_2,
                    child: Text(
                      'Scandinavian small sized double sofa imported full leather / Dale Italia oil wax leather black',
                      textAlign: TextAlign.left,
                      style: TextStyle(color: Colors.grey, fontSize: 12.0),
                    ),
                  ),
                  SizedBox(height: 10.0),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Material(
                        elevation: isFavorite ? 0.0 : 2.0,
                        borderRadius: BorderRadius.circular(20.0),
                        child: InkWell(
                          onTap: () {
                            setState(() {
                              item.fav = !item.fav;
                            });
                          },
                          child: Container(
                            // margin: EdgeInsets.only(right: 10.0),
                            height: 40.0,
                            width: 40.0,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20.0),
                                color: isFavorite
                                    ? Colors.grey.withOpacity(0.2)
                                    : Colors.white),
                            child: Center(
                              child: isFavorite
                                  ? Icon(Icons.favorite_border)
                                  : Icon(Icons.favorite, color: Colors.red),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 10.0,
                      ),
                      Container(
                        height: 40.0,
                        width: 50.0,
                        color: Colors.pink[300],
                        child: Center(
                          child: Text(
                            '\$248',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                      Container(
                        height: 40.0,
                        width: 100.0,
                        color: Colors.pink[200],
                        child: Center(
                          child: Text(
                            'Add to cart',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
