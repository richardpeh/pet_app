import 'package:flutter/material.dart';
import 'dart:convert';
import 'dart:io';
import 'package:percent_indicator/percent_indicator.dart';

class PetCameraPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _PetCameraPageState();
  }
}

class _Config {
  _TYPE type;
  _TAB tab;
  List<String> payload;

  _Config(this.type, this.tab, this.payload);
}

enum _TYPE { CAMERA, ADS, WEATHER, GPS, TOLL }

enum _TAB { HOME, CAM1, CAM2, ALL }

var _PAYLOAD__HOME = {
  'URL': 0,
  'TEXT': 1,
};

var _PAYLOAD__CAM1 = {
  'URL': 0,
  'TEXT': 1,
};

var _PAYLOAD__CAM2 = {
  'URL': 0,
  'TEXT': 1,
};

class _PetCameraPageState extends State<PetCameraPage>
    with TickerProviderStateMixin {
  TabController _controller;

  // Animation
  Path _path_b1 = Path();
  Path _path_b2 = Path();
  Animation<double> animation;
  AnimationController _acontroller;

  final double _bridge1_x_max = 100.0;
  final double _bridge1_x_threshold = 95.0;
  final double _bridge1_y_max = 100.0;
  final double _bridge2_x_max = 100.0;
  final double _bridge2_x_threshold = 145.0;
  final double _bridge2_y_max = 100.0;

  String remoteConfigUrl;
  String remoteConfigRaw;

  List<_Config> localConfigList = []
    ..add(_Config(_TYPE.CAMERA, _TAB.CAM1, [
      'http://vigroot.llm.gov.my/vigroot/cam_root/web/PNB/PNB_CAM_06_PNG_BRG_KM0.30_WB.web.jpg',
      'PNB_CAM_06_PNG_BRG_KM0.30'
    ]))
    ..add(_Config(_TYPE.CAMERA, _TAB.CAM1, [
      'http://vigroot.llm.gov.my/vigroot/cam_root/web/PNB/PNB_CAM_08_PNG_BRG_KM1.60_WB.web.jpg',
      'PNB_CAM_08_PNG_BRG_KM1.60'
    ]))
    ..add(_Config(_TYPE.CAMERA, _TAB.CAM1, [
      'http://vigroot.llm.gov.my/vigroot/cam_root/web/PNB/PNB_CAM_09_PNG_BRG_KM2.80_EB.web.jpg',
      'PNB_CAM_09_PNG_BRG_KM2.80'
    ]))
    ..add(_Config(_TYPE.CAMERA, _TAB.CAM2, [
      'http://vigroot.llm.gov.my/vigroot/cam_root/web/PB2/PB2_CAM_03_P1.5_WB_PLUS.web.jpg',
      'PB2_CAM_03_P1.5_WB_PLUS'
    ]))
    ..add(_Config(_TYPE.CAMERA, _TAB.CAM2, [
      'http://vigroot.llm.gov.my/vigroot/cam_root/web/PB2/PB2_CAM_06_KM1.8_EB.web.jpg',
      'PB2_CAM_06_KM1.8_EB'
    ]))
    ..add(_Config(_TYPE.CAMERA, _TAB.CAM2, [
      'http://vigroot.llm.gov.my/vigroot/cam_root/web/PB2/PB2_CAM_16_KM12.7_MEDIAN.web.jpg',
      'PB2_CAM_16_KM12.7_MEDIAN'
    ]));

  List<Card> cardList__HOME = [];
  List<Card> cardList__CAM1 = [];
  List<Card> cardList__CAM2 = [];

  @override
  void initState() {
    super.initState();
    _controller = new TabController(length: 4, vsync: this);
    _acontroller = new AnimationController(
        vsync: this, duration: Duration(milliseconds: 5000));

    animation = Tween(begin: 0.0, end: _bridge1_x_max).animate(_acontroller)
      ..addListener(() {
        setState(() {
          if (animation.value > _bridge1_x_threshold) {
            initPath();
          }
        });
        _refresh_home();
      });
    _acontroller.repeat();

    initPath();

    _fetchRemoteConfig();
  }

  void initPath() {
    _path_b1.reset();
    _path_b2.reset();
    _path_b1.moveTo(0.0, 0.0);
    _path_b2.moveTo(50.0, 50.0);
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    _parseConfig(_TAB.ALL);
  }

  void _fetchRemoteConfig() async {
    // await _fetchRemoteConfigFromServer();
  }

  Future _fetchRemoteConfigFromServer() async {
    // if (remoteConfigUrl != null) {
    //   return;
    // }

    // HttpClient http = HttpClient();
    // try {
    //   var uri = Uri.http(
    //       localConfigList[0].payload[0], localConfigList[0].payload[1]);
    //   var request = await http.getUrl(uri);
    //   var response = await request.close();
    //   var responseBody = await response.transform(utf8.decoder).join();

    //   // The dog.ceo API returns a JSON object with a property
    //   // called 'message', which actually is the URL.
    //   remoteConfigUrl = json.decode(responseBody)['message'];
    //   print(remoteConfigUrl);
    // } catch (exception) {
    //   print(exception);
    // }
  }

  void _parseConfig(_TAB level) {
    // Populate localConfig using remoteConfig
    //   Currently very wasteful on the forloops, optimize later
    if (level == _TAB.HOME || level == _TAB.ALL) {
      // Hack home page
      cardList__HOME = []
        ..add(_card_sample())
        ..add(_card_sample5())
        ..add(_card_sample3())
        ..add(_card_sample2());

      // Generate the Card List
      for (_Config conf in localConfigList) {
        if (conf.tab == _TAB.HOME) {
          Card card = _card_camera(
              conf.payload[_PAYLOAD__HOME['URL']] + '?${DateTime.now()}',
              conf.payload[_PAYLOAD__HOME['TEXT']]);
          cardList__HOME.add(card);
        }
      }
    }

    if (level == _TAB.CAM1 || level == _TAB.ALL) {
      // Generate the Card List
      for (_Config conf in localConfigList) {
        if (conf.tab == _TAB.CAM1) {
          Card card = _card_camera(
              conf.payload[_PAYLOAD__CAM1['URL']] + '?${DateTime.now()}',
              conf.payload[_PAYLOAD__CAM1['TEXT']]);
          cardList__CAM1.add(card);
        }
      }
    }

    if (level == _TAB.CAM2 || level == _TAB.ALL) {
      // Generate the Card List
      for (_Config conf in localConfigList) {
        if (conf.tab == _TAB.CAM2) {
          Card card = _card_camera(
              conf.payload[_PAYLOAD__CAM2['URL']] + '?${DateTime.now()}',
              conf.payload[_PAYLOAD__CAM2['TEXT']]);
          cardList__CAM2.add(card);
        }
      }
    }

    setState(() {
      // print('pwc set state');
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    _acontroller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[200],
        appBar: AppBar(
          title: Text('Pet Camera'),
          backgroundColor: Colors.pink,
          actions: [
            IconButton(
              icon: Icon(Icons.settings),
              onPressed: () {},
            ),
          ],
        ),
        body: TabBarView(
          controller: _controller,
          children: <Widget>[
            _myHomeView,
            _pg1View,
            _pg2View,
          ],
        ),
        bottomNavigationBar: SafeArea(
          child: Material(
            color: Colors.grey[200],
            child: TabBar(
              controller: _controller,
              tabs: <Widget>[
                Tab(
                  icon: new Icon(Icons.home),
                ),
                Tab(
                  icon: new Icon(Icons.crop_din),
                ),
                Tab(
                  icon: new Icon(Icons.crop_din),
                ),
              ],
              labelColor: Colors.pink,
              unselectedLabelColor: Colors.grey,
              indicatorSize: TabBarIndicatorSize.tab,
              // indicatorPadding: EdgeInsets.all(5.0),
              indicatorColor: Colors.pink,
              indicator: UnderlineTabIndicator(
                borderSide: BorderSide(color: Colors.pink, width: 2.0),
                insets: EdgeInsets.fromLTRB(50.0, 0.0, 50.0, 40.0),
              ),
            ),
          ),
        ));
  }

  Future<void> _refresh_home() async {
    setState(() {
      cardList__HOME.clear();
      _parseConfig(_TAB.HOME);
    });
  }

  Future<void> _refresh_cam1() async {
    setState(() {
      cardList__CAM1.clear();
      _parseConfig(_TAB.CAM1);
    });
  }

  Future<void> _refresh_cam2() async {
    setState(() {
      cardList__CAM2.clear();
      _parseConfig(_TAB.CAM2);
    });
  }

  RefreshIndicator get _myHomeView {
    return RefreshIndicator(
        onRefresh: _refresh_home,
        child: ListView.builder(
          itemCount: cardList__HOME.length,

          // A callback that will return a widget.
          itemBuilder: (context, i) {
            return cardList__HOME[i];
          },
        ));
  }

  RefreshIndicator get _pg1View {
    return RefreshIndicator(
        onRefresh: _refresh_cam1,
        child: ListView.builder(
          itemCount: cardList__CAM1.length,

          // A callback that will return a widget.
          itemBuilder: (context, i) {
            return cardList__CAM1[i];
          },
        ));
  }

  RefreshIndicator get _pg2View {
    return RefreshIndicator(
        onRefresh: _refresh_cam2,
        child: ListView.builder(
          itemCount: cardList__CAM2.length,

          // A callback that will return a widget.
          itemBuilder: (context, i) {
            return cardList__CAM2[i];
          },
        ));
  }

  Card _card_camera(String url, String text) {
    return Card(
      margin: EdgeInsets.only(left: 15.0, right: 15.0, top: 15.0),
      elevation: 2.0,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width - 30,
                child: _card_camera_animation(url),
              ),
            ],
          ),
          ListTile(title: Text(text)),
        ],
      ),
    );
  }

  Widget _card_camera_animation(String url) {
    var petAvatar = Image(
      image: NetworkImage(url ?? ''),
      fit: BoxFit.fitWidth,
      width: MediaQuery.of(context).size.width - 30,
    );

    // Placeholder is a static container same size as petAvatar
    var placeholder = FlutterLogo(
      size: 1.0,
    );

    return AnimatedCrossFade(
      firstChild: placeholder,
      secondChild: petAvatar,
      crossFadeState:
          (url == null) ? CrossFadeState.showFirst : CrossFadeState.showSecond,
      duration: Duration(milliseconds: 500),
    );
  }

  Card _card_sample2() {
    return Card(
      margin: EdgeInsets.only(left: 15.0, right: 15.0, top: 15.0),
      elevation: 2.0,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(
            // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            // mainAxisSize: MainAxisSize.min,r
            children: <Widget>[
              Container(
                child: Image(
                  image: AssetImage('assets/cam.jpg'),
                  fit: BoxFit.fitWidth,
                ),
                width: MediaQuery.of(context).size.width - 30,
              ),
            ],
          ),
          ListTile(title: Text("This is a title")),
        ],
      ),
    );
  }

  Card _card_sample3() {
    return Card(
      margin: EdgeInsets.only(left: 15.0, right: 15.0, top: 15.0),
      elevation: 2.0,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Stack(
            alignment: Alignment(0, -1.0),
            children: <Widget>[
              // Container(
              //   child: Image(
              //     image: AssetImage('assets/pg1.jpg'),
              //     fit: BoxFit.fitWidth,
              //   ),
              //   width: MediaQuery.of(context).size.width - 30,
              //   // height: 180.0
              // ),
              Container(
                  color: Colors.blueGrey,
                  width: MediaQuery.of(context).size.width - 30,
                  height: (MediaQuery.of(context).size.width - 30) / 2),
              Container(
                  alignment: Alignment(-1.0, 1),
                  padding: EdgeInsets.only(left: 20.0, top: 25.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Penang Bridge",
                        style: TextStyle(color: Colors.white, fontSize: 24),
                      ),
                      // SizedBox(height: 10.0,),

                      Text(
                        "------",
                        style: TextStyle(color: Colors.white, fontSize: 18),
                      ),
                      // SizedBox(height: 10.0,),
                      Text(
                        "SUNNY",
                        style: TextStyle(color: Colors.white, fontSize: 14),
                      ),
                    ],
                  )),
              Positioned(
                bottom: 25.0,
                left: 20.0,
                child: Text(
                  "32'C",
                  style: TextStyle(color: Colors.white, fontSize: 30),
                ),
              ),
            ],
          ),
          Container(
            width: MediaQuery.of(context).size.width - 30,
            padding: EdgeInsets.all(15.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Text(
                  "PG -> SP",
                  style: TextStyle(fontSize: 18.0, color: Colors.black),
                ),
                Text(
                  "SP <- PG",
                  style: TextStyle(fontSize: 18.0, color: Colors.black),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.all(15.0),
            child: LinearPercentIndicator(
              width: MediaQuery.of(context).size.width - 150,
              animation: true,
              lineHeight: 5.0,
              animationDuration: 10000,
              percent: 0.8,
              // center: Text("80.0%"),
              linearStrokeCap: LinearStrokeCap.roundAll,
              progressColor: Colors.pink,
            ),
          ),
        ],
      ),
    );
  }

  Card _card_sample() {
    return Card(
      margin: EdgeInsets.only(left: 15.0, right: 15.0, top: 15),
      elevation: 2.0,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const ListTile(
            leading: Icon(Icons.map),
            title: Text('Welcome, Richard Peh'),
            subtitle: Text('Music by Julie Gable. Lyrics by Sidney Stein.'),
          ),
          _button_ThemeBar()
        ],
      ),
    );
  }

  List<BridgeAnimationData> get populateBridgeAnimationData {
    List<BridgeAnimationData> data = []
      ..add(BridgeAnimationData(
          _path_b1, animation.value, 0.0, _bridge1_x_threshold, Colors.pink))
      ..add(BridgeAnimationData(
          _path_b2, animation.value + 50, 50.0, _bridge2_x_threshold, Colors.blue));

    return data;
  }

  Card _card_sample5() {
    return Card(
      margin: EdgeInsets.only(left: 15.0, right: 15.0, top: 15.0),
      elevation: 2.0,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          CustomPaint(
            foregroundPainter: MyPainter(populateBridgeAnimationData),
            child: Container(
              child: Image(
                image: AssetImage('assets/map.jpg'),
                fit: BoxFit.fitWidth,
              ),
              width: MediaQuery.of(context).size.width - 30,
            ),
          ),
          ListTile(title: Text("This is a title")),
        ],
      ),
    );
  }

  Widget _button_ThemeBar() {
    return ButtonTheme.bar(
      child: ButtonBar(
        children: <Widget>[
          FlatButton(
            child: const Text('ROUTE'),
            onPressed: () {},
          ),
          FlatButton(
            child: const Text('GO'),
            onPressed: () {},
          ),
        ],
      ),
    );
  }
}

class BridgeAnimationData {
  Path path;
  double x;
  double y;
  double threshold;
  Color color;

  BridgeAnimationData(this.path, this.x, this.y, this.threshold, this.color);
}

class MyPainter extends CustomPainter {
  List<BridgeAnimationData> list;

  MyPainter(this.list);

  @override
  void paint(Canvas canvas, Size size) {
    for (BridgeAnimationData b in list) {
      // if (b.color != Colors.pink)
      if (b.x < b.threshold) {
        b.path.lineTo(b.x, b.y);
        canvas.drawPath(
            b.path,
            Paint()
              ..color = b.color
              ..strokeCap = StrokeCap.round
              ..style = PaintingStyle.stroke
              ..strokeWidth = 5.0);
        // print("${b.color} X:${b.x} Y:${b.y}");
      }
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
