import 'package:flutter/material.dart';
import 'package:pet_app/pet_theme.dart';
import 'package:pet_app/pet_model.dart';

class AddPetFormPage extends StatefulWidget {
  // final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  State<StatefulWidget> createState() => _AddPetFormPageState();
}

class _AddPetFormPageState extends State<AddPetFormPage> {
  TextEditingController nameController = TextEditingController();
  TextEditingController locationController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    // print(_scaffoldKey);
    // print(widget._scaffoldKey);
    // nameController.addListener(()=>print(nameController.text));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text('Add a new pet'),
          backgroundColor: Colors.pink,
        ),
        body: Container(
            color: Colors.white10,
            child: Padding(
              padding: getPadding(8.0, 32.0),
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: TextField(
                      controller: nameController,
                      decoration: InputDecoration(
                        labelText: 'Name the pet',
                        prefixIcon: Icon(
                          Icons.pets,
                          color: Colors.pink,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: TextField(
                      controller: locationController,
                      decoration: InputDecoration(
                        labelText: "Pet's location",
                        prefixIcon: Icon(
                          Icons.location_city,
                          color: Colors.pink,
                        ),
                      ),
                    ),
                  ),

                  Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: TextField(
                      controller: descriptionController,
                      decoration: InputDecoration(
                        labelText: "Pet's description",
                        prefixIcon: Icon(
                          Icons.details,
                          color: Colors.pink,
                        ),
                      ),
                    ),
                  ),

                  // A Strange situation.
                  // A piece of the app that you'll add in the next
                  // section *needs* to know its context,
                  // and the easiest way to pass a context is to
                  // use a builder method. So I've wrapped
                  // this button in a Builder as a sort of 'hack'.
                  Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: RaisedButton(
                        onPressed: () => submitPet(),
                        color: Colors.pink,
                        child: Text('Submit pet', style: TextStyle(color: Colors.white),),
                      )),
                ],
              ),
            )));
  }

  void submitPet() {
    if (nameController.text.isEmpty) {
      // Scaffold.of(context).showSnackBar(
      //   SnackBar(
      //     backgroundColor: Colors.purple,
      //     content: Text('Pet need names!'),
      //   ),
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        backgroundColor: Colors.pink,
        content: Text('Pet need names!'),
      ));
    } else {
      // Create a new dog with the information from the form.
      var newPet = Pet(nameController.text, locationController.text,
          descriptionController.text);
      // Pop the page off the route stack and pass the new
      // dog back to wherever this page was created.
      Navigator.of(context).pop(newPet);
    }
  }
}
