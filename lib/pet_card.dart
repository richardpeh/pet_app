import 'package:flutter/material.dart';
import 'pet_detail_page.dart';
import 'pet_model.dart';

class PetCard extends StatefulWidget {
  final Pet pet;

  PetCard(this.pet);

  @override
  _PetCardState createState() => _PetCardState();
}

class _PetCardState extends State<PetCard> {
  String renderUrl;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: showPetDetailPage,
      // child: Container(
      // height: 120,
      // width: double.infinity,
      // child: Stack(
      //   children: <Widget>[
      //     Positioned(
      //       top: 10,
      //       left: 80.0,
      //       child: petCard,
      //     ),
      //     Positioned(
      //       top: 5.0,
      //       left: 5,
      //       child: petImage,
      //     ),
      //   ],
      // ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        // crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Stack(
            children: <Widget>[
              SizedBox(
                height: 120,
                width: 370.0,           
              ),
              Positioned(
                top: 10,
                left: 80.0,
                child: petCard,
              ),
              Positioned(
                top: 5.0,
                left: 5,
                child: petImage,
              ),
            ],
          ),
        ],
      ),
      // )
    );
  }

  void showPetDetailPage() {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return PetDetailPage(widget.pet);
    }));
  }

  @override
  void initState() {
    super.initState();
    renderPetPic();
  }

  void renderPetPic() async {
    await widget.pet.getImageUrl();

    setState(() {
      renderUrl = widget.pet.imageUrl;
    });
  }

  Widget get petImage {
    var petAvatar = Hero(
      tag: widget.pet,
      child: Container(
        width: 110.0,
        height: 110.0,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
            fit: BoxFit.cover,
            image: NetworkImage(renderUrl ?? ''),
          ),
        ),
      ),
    );

    // Placeholder is a static container same size as petAvatar
    var placeholder = Container(
      width: 110.0,
      height: 110.0,
      decoration: BoxDecoration(
          shape: BoxShape.circle,
          gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            stops: [0.1, 0.9],
            colors: [
              Colors.white,
              Colors.white70,
            ],
          )),
      alignment: Alignment.center,
      child: Text(
        'PET',
        textAlign: TextAlign.center,
      ),
    );

    return AnimatedCrossFade(
      firstChild: placeholder,
      secondChild: petAvatar,
      crossFadeState: (renderUrl == null)
          ? CrossFadeState.showFirst
          : CrossFadeState.showSecond,
      duration: Duration(milliseconds: 1000),
    );
  }

  Widget get petCard {
    return Container(
      width: 290.0,
      height: 100.0,
      child: Card(
        color: Colors.white30,
        child: Padding(
            padding: const EdgeInsets.only(
              top: 8.0,
              bottom: 8.0,
              left: 45.0,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Text(widget.pet.name,
                    style: Theme.of(context).textTheme.headline),
                Text(widget.pet.location,
                    style: Theme.of(context).textTheme.subhead),
                Row(
                  children: <Widget>[
                    Icon(
                      Icons.star,
                    ),
                    Text(': ${widget.pet.rating} /10'),
                  ],
                )
              ],
            )),
      ),
    );
  }
}
